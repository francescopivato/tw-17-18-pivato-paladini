$(function(){

  $.getJSON("listaAlimenti.json", function(data, status){

    $('#listino').empty();

    var allFood = [];

    for (var i = 0; i < data.length; i++) {

      allFood.push({
        "id" : parseInt(data[i].id), // id from sql is saved as string. parseInt converts it into a number
        "nome" : data[i].nome,
        "rimanenze" : data[i].rimanenze,
        "quantitadaordinare" : 0
      });

      var id = parseInt(data[i].id); // id from sql is saved as string. parseInt converts it into a number
      var nome = data[i].nome;
      var rimanenze = parseInt(data[i].rimanenze);

      var newli ="<li class=\"listino-item\" id=\"listino-item" + id + "\"></li>";

      $( "#listino" ).append( newli );

      var liform = "<form class=\"listino-item-form\" id=\"listino-item-form"+ id +"\" action=\"listino_script.php\" method=\"post\"></form>";

      $("#listino-item" + id).append(liform);

      var divli_id = "<div class=\"div-listino-item divli-id\"></div>";

      var divli_nome = "<div class=\"div-listino-item divli-nome\"></div>";

      var divli_rimanenze = "<div class=\"div-listino-item divli-rimanenze\"></div>";

      var divli_quantitadaordinare = "<div class=\"div-listino-item divli-quantitadaordinare\"></div>";

      var divli_bottoni = "<div class=\"div-listino-item divli-bottoni\"></div>";

      // var a_ordinaalimento = "<a href=\"#\" class=\"btn btn-dark listino-item-ordina\">Ordina Alimento</a>";
      //
      // var a_rimuovialimento = "<a href=\"#\" class=\"btn btn-dark listino-item-rimuovi a-rimuovialimento\">Rimuovi Alimento</a>";

      $("#listino-item-form" + id).append(divli_id, divli_nome, divli_rimanenze, divli_quantitadaordinare, divli_bottoni);

      $("#listino-item-form" + id + " > .divli-id").append("<label for=\"id\">ID: </label><input type=\"text\" id=\"id\" name=\"id\" value=\"" + id + "\" class=\"listino-item-id\" readonly>");

      $("#listino-item-form" + id + " > .divli-nome").append("<label for=\"nome\">Nome: </label><input type=\"text\" id=\"nome\" name=\"nome\" value=\"" + nome + "\" class=\"listino-item-nome\" readonly>");

      $("#listino-item-form" + id + " > .divli-rimanenze").append("<label for=\"rimanenze\">Rimanenze: </label><input type=\"text\" id=\"rimanenze\" name=\"rimanenze\" value=\"" + rimanenze + "\" class=\"listino-item-rimanenze\" readonly>");

      $("#listino-item-form" + id + " > .divli-quantitadaordinare").append("<label for=\"quantitadaordinare\">Quantità da ordinare: </label><input type=\"number\" min=\"0\" max=\"100\" id=\"quantitadaordinare\" name=\"quantitadaordinare\" class=\"listino-item-quantitadaordinare\" value=\"0\">");

      $("#listino-item-form" + id + " > .divli-bottoni").append("<input class=\"btn btn-dark listino-item-ordina\" type=\"submit\" name=\"ordinaAlimento\" value=\"Ordina\"><input class=\"btn btn-dark listino-item-rimuovi\" type=\"submit\" name=\"rimuoviAlimento\" value=\"Rimuovi\">");

      // <a> is already appended so the following row is not needed
      // $("#listino-item" + id + " > .a-rimuovialimento").append("<a href=\"#\" class=\"btn btn-dark listino-item-rimuovi\">Rimuovi Alimento</a>");

    } // close for

    $(".listino-item-quantitadaordinare").change(function(){ //il numero inserito deve essere compreso tra 0 e 100

      if ($(this).val() < 0) {

        $(this).val(0);
        $(this).attr("value", 0);

      } else {

        if ($(this).val() > 100) {

          $(this).val(100);
          $(this).attr("value", 100);

        } else {

          $(this).attr("value", $(this).val());

        }
      }
    });

  }); // close getJSON
}); // close function
