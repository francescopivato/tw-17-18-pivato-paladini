    <?php

      $array = [];
      $fileName = 'listaAlimenti.json';

      require("db_connection.php");
      require("use_db.php");

      $query = "SELECT * FROM alimenti";

      $result = $conn->query($query);

      if ($result->num_rows>0) {
        while ($row = $result->fetch_assoc()) {
          $array[] = $row;
        }
      }

      // print_r($array);

      $fp = fopen($fileName, 'w');
      fwrite($fp, json_encode($array, JSON_PRETTY_PRINT));
      fclose($fp);

     ?>
