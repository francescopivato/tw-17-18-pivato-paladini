<?php

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  require("db_connection.php");
  require("use_db.php");

  $id_current_user = $_SESSION['id'];
  $unread = 0;

  $stmt_select = $conn->prepare("SELECT utente_id, messaggio, letto FROM notifiche WHERE utente_id=? AND letto=?");

  $stmt_select->bind_param("ii", $id_current_user, $unread);
  $stmt_select->execute();
  $stmt_select->store_result();
  $stmt_select->bind_result($id, $messaggio, $letto);
  $stmt_select->fetch();

  if ($stmt_select->num_rows>0) {
    echo "Hai una nuova notifica. Seleziona Notifiche dal menu per leggere il messaggio.";
  } else {
    echo "false";
  }

  $conn->close();

?>
