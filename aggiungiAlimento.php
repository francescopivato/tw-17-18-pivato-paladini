<?php
  if (session_status() == PHP_SESSION_NONE) { session_start(); }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aggiungi Alimento</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="controlla_notifiche.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="home-header">
            <figure>
              <img class="logo desktop" src="onlinefoodlogo.png" alt="Online Food Logo">
              <figcaption></figcaption>
            </figure>
            <figure>
              <img class="logo mobile" src="onlinefoodlogoMobile.png" alt="Online Food Logo">
              <figcaption></figcaption>
            </figure>
            <h1>Online Food</h1>
          </header>
        </div>
      </div>
      <div class="row nav">
        <div class="offset-sm-1 col-sm-10">
          <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
              <img src="onlinefoodlogoMobile.png" width="50" height="50" class="mx-auto" alt=""> Online Food
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <a class="nav-item nav-link" href="home_admin.php">Home</a>
              <a class="nav-item nav-link" href="notifiche_admin.php">Notifiche</a>
              <a class="nav-item nav-link" href="listino.php">Listino</a>
              <a class="nav-item nav-link" href="logout.php">Logout</a>
            </div>
          </nav>
        </div>
      </div>
      <div class="row main">
        <div class="col-sm-12">
          <main>
            <div class="row formalimenti">
              <div class="offset-sm-2 col-sm-8">
                  <section>
                    <h2 class="titoloPagina">Aggiungi Alimento</h2>
                    <form id="formAggiungiAlimento" action="aggiungiAlimento_script.php" name="formAggiungiAlimento" method="post">
                      <div class="divAggiungiAlimento">
                        <label for="nome" class="sr-only">Nome (max 30 caratteri): </label>
                        <input type="text" id="nome" name="nome" maxlength="30" placeholder="Nome (max 30 caratteri)" required>
                      </div>
                      <div class="divAggiungiAlimento">
                        <label for="descrizione" class="sr-only">Descrizione (max 300 caratteri): </label>
                        <!-- <input type="textarea" name="descrizione" value="" maxlength="300" placeholder="Descrizione (max 300 caratteri)"> -->
                        <textarea id="descrizione" name="descrizione" rows="8" cols="80" maxlength="300" placeholder="Descrizione (max 300 caratteri)" required></textarea>
                      </div>
                      <div class="divAggiungiAlimento">
                        <label for="ingredienti" class="sr-only">Ingredienti (max 300 caratteri): </label>
                        <!-- <input type="text" name="ingredienti" value="" > -->
                        <textarea id="ingredienti" name="ingredienti" rows="8" cols="80" maxlength="300" placeholder="Ingredienti (max 300 caratteri)" required></textarea>
                      </div>
                      <div class="divAggiungiAlimento">
                        <label for="tipocucina" class="sr-only">Tipo Cucina (es: Italiana... ): </label>
                        <input type="text" id="tipocucina" name="tipocucina" value="" placeholder="Tipo Cucina (es: Italiana... )" required>
                      </div>
                      <div class="divAggiungiAlimento">
                        <label for="prezzo" class="sr-only">Prezzo: </label>
                        <input type="number" id="prezzo" name="prezzo" min="0" value="" step=".01" placeholder="Prezzo (00.00)" required>
                      </div>
                      <div class="divAggiungiAlimento">
                        <label for="rimanenze" class="sr-only">Rimanenze: </label>
                        <input type="number" id="rimanenze" name="rimanenze" min="0" max="100" value="" placeholder="Rimanenze" required>
                      </div>
                      <div class="divAggiungiAlimento">
                        <input id="submitAggiungiAlimento" type="submit" class="btn btn-primary" name="submitAggiungiAlimento" value="Aggiungi Alimento">
                      </div>
                    </form>
                  </section>
              </div>
            </div>
          </main>
        </div>
      </div>
<div class="row footer">
  <div class="col-sm-12">
    <footer>
      Sito sviluppato da Pivato Francesco
    </footer>
  </div>
</div>
    </div>
  </body>
</html>
