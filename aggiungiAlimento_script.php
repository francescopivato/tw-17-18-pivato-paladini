<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

if (isset($_POST['nome']) &&
    isset($_POST['descrizione']) &&
    isset($_POST['ingredienti']) &&
    isset($_POST['tipocucina']) &&
    isset($_POST['prezzo']) &&
    isset($_POST['rimanenze']))   {

    require("db_connection.php");
    require("use_db.php");

    $nome = $_POST['nome'];
    $descrizione = $_POST['descrizione'];
    $ingredienti = $_POST['ingredienti'];
    $tipocucina = $_POST['tipocucina'];
    $prezzo = $_POST['prezzo'];
    $rimanenze = $_POST['rimanenze'];

    $stmt = $conn->prepare("INSERT INTO alimenti (nome, descrizione, ingredienti, tipocucina, prezzo, rimanenze)
                              VALUES (?, ?, ?, ?, ?, ?)");

    $stmt->bind_param("ssssss", $nome, $descrizione, $ingredienti, $tipocucina, $prezzo, $rimanenze);

    if ($stmt->execute()) {
      $stmt_insert_notification = $conn->prepare("INSERT INTO notifiche (utente_id, messaggio, letto, data) VALUES (?, ?, ?, ?)");

      date_default_timezone_set("Europe/Rome");

      $id_user = $_SESSION['id'];
      $message = "Nuovo alimento " . $nome . " con porzioni pari a " . $rimanenze . " aggiunto al magazzino.";
      $letto = 0;
      $data = date("Y-m-d") . " " . date("G:i:s");

      $stmt_insert_notification->bind_param("isis", $id_user, $message, $letto, $data);
      $stmt_insert_notification->execute();
      $stmt_insert_notification->close();
    }

    $stmt->close();
    $conn->close();

    header('Location: '. $_SERVER['HOST_NAME'] . '/tw-17-18-pivato-paladini/home_admin.php');
  }
?>
