<?php

    ob_start();

    if (session_status() == PHP_SESSION_NONE) { session_start(); }

    // print_r($_SESSION);

    require("db_connection.php");
    require("use_db.php");

      if (isset($_POST['myCart'])) {

        //By default, json_encode will create stdClass objects like the above.
        //If you would prefer an associative array, pass true as the second parameter to json_decode
        $myCart = json_decode($_POST['myCart'], true);

        if (count($myCart) > 0) { // if my cart is not empty, do stuff

          $stmt_select = $conn->prepare("SELECT rimanenze FROM alimenti WHERE id=?");
          $stmt_update = $conn->prepare("UPDATE alimenti SET rimanenze=? WHERE id=?");
          $stmt_insert_notification = $conn->prepare("INSERT INTO notifiche (utente_id, messaggio, letto, data) VALUES (?, ?, ?, ?)");

            foreach ($myCart as $key=>$value) {
              foreach ($value as $k => $v) {
                $quantita_magazzino = 0;
                if($k == 'id'){
                  $result;
                  $id = $v;
                  $stmt_select->bind_param("i", $id);
                  $stmt_select->execute();
                  $stmt_select->bind_result($result);
                  $stmt_select->fetch();
                  $stmt_select->store_result();
                  $rimanenze_magazzino = $result;
                }
                if ($k == 'quantita') {
                  $quantita = $v;
                }
              } // close internal foreach
                $rimanenze_totali = ($rimanenze_magazzino - $quantita);
                $stmt_select->free_result();
                $stmt_update->bind_param("ii", $rimanenze_totali, $id);
                $stmt_update->execute();
                $stmt_update->store_result();
                $stmt_update->free_result();

          } // close external foreach

          date_default_timezone_set("Europe/Rome");

          $id_user = $_SESSION['id'];
          $message = "Ordine in preparazione.";
          $letto = 0;
          $data = date("Y-m-d") . " " . date("G:i:s");

          $stmt_insert_notification->bind_param("isis", $id_user, $message, $letto, $data);
          $stmt_insert_notification->execute();
          $stmt_insert_notification->store_result();
          $stmt_insert_notification->free_result();

          $stmt_select->close();
          $stmt_update->close();
          $stmt_insert_notification->close();

          $_SESSION['ordine']=true;
        }
      }

      $conn->close();

      // segnala l'ordine all'amministratore
      // segnala l'ordine all'utente nella relativa pagina
      // parte timer alla scadenza del quale arriva notifica all'utente che la spedizione è partita
      // timer avvisa anche quando la spedizione è arrivata

      header('Location: '. $_SERVER['HOST_NAME'] . '/tw-17-18-pivato-paladini/home_user.php');

      ob_end_flush();

?>
