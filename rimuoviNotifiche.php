<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

require("db_connection.php");
require("use_db.php");

$id_current_user = $_SESSION['id'];

$stmt_delete = $conn->prepare("DELETE FROM notifiche WHERE utente_id=?");

$stmt_delete->bind_param("i", $id_current_user);
$stmt_delete->execute();

$stmt_delete->close();
$conn->close();

if ($id_current_user == 1) { // if admin
  header('Location: '. $_SERVER['HOST_NAME'] . '/tw-17-18-pivato-paladini/notifiche_admin.php');
} else {
  header('Location: '. $_SERVER['HOST_NAME'] . '/tw-17-18-pivato-paladini/notifiche_user.php');
}

?>
