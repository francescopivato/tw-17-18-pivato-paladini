<?php if (session_status() == PHP_SESSION_NONE) { session_start(); } ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Carrello</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="carrello.js"></script>
    <script type="text/javascript" src="controlla_notifiche.js"></script>
    <script type="text/javascript">
      $(function(){
        $("#logout").click(function(){
          if (localStorage.getItem("myCart") != null) {
            localStorage.removeItem("myCart");
          }
        });
      });
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="home-header">
            <figure>
              <img class="logo desktop" src="onlinefoodlogo.png" alt="Online Food Logo">
              <figcaption></figcaption>
            </figure>
            <figure>
              <img class="logo mobile" src="onlinefoodlogoMobile.png" alt="Online Food Logo">
              <figcaption></figcaption>
            </figure>
            <h1>Online Food</h1>
          </header>
        </div>
      </div>
      <div class="row nav">
        <div class="offset-sm-1 col-sm-10">
          <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
              <img src="onlinefoodlogoMobile.png" width="50" height="50" class="mx-auto" alt=""> Online Food
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <a class="nav-item nav-link" href="home_user.php">Home</a>
              <a class="nav-item nav-link" href="notifiche_user.php">Notifiche</a>
              <a class="nav-item nav-link" href="ordini.php">Ordini</a>
              <a class="nav-item nav-link" href="#">Carrello</a>
              <a class="nav-item nav-link" id="logout" href="logout.php">Logout</a>
            </div>
          </nav>
        </div>
      </div>
      <div class="row main">
        <div class="col-sm-12">
            <div class="offset-sm-1 col-sm-10">
              <main>
                <h2 class="titoloPagina">Carrello</h2>
                <ul id="listaCarrello"></ul>
                <h3 id="totalPrice">Prezzo totale: 0€</h3>
                <form method="post" action="pagamento.php" id="formpagamento">
                  <label for="destinazione">Spedisci a: </label>
                  <input type="radio" id="destinazione" value="campusCesena" class="form-check" checked> Campus di Cesena
                  <div class="bottonipagamento">
                    <button type="submit" class="btn btn-primary" name="pagamento" id="tasto_pagamento" value="Paga">Procedi al pagamento</button>
                    <button type="button" class="btn btn-primary" name="button" id="stamparicevuta" >Stampa la ricevuta</button>
                  </div>
                </form>
            </div>
            </main>
        </div>
      </div>
<div class="row footer">
  <div class="col-sm-12">
    <footer>
      Sito sviluppato da Pivato Francesco
    </footer>
  </div>
</div>
    </div>
  </body>
</html>
