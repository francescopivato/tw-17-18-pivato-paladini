<?php
  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  require("selezionaAlimenti.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript">
    $(document).ajaxComplete(function() {
      $("[data-toggle=popover]").popover(); //needed to initialize popovers
    });
    </script>
    <script type="text/javascript" src="selezionaCibo.js"></script>
    <script type="text/javascript" src="controlla_notifiche.js"></script>
    <script type="text/javascript">
      $(function(){
        $("#logout").click(function(){
          if (localStorage.getItem("myCart") != null) {
            localStorage.removeItem("myCart");
          }
        });
      });
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="home-header">
            <figure>
              <img class="logo desktop" src="onlinefoodlogo.png" alt="Online Food Logo">
              <figcaption></figcaption>
            </figure>
            <figure>
              <img class="logo mobile" src="onlinefoodlogoMobile.png" alt="Online Food Logo">
              <figcaption></figcaption>
            </figure>
            <h1>Online Food</h1>
          </header>
        </div>
      </div>
      <div class="row nav">
        <div class="offset-sm-1 col-sm-10">
          <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
              <img src="onlinefoodlogoMobile.png" width="50" height="50" class="mx-auto" alt=""> Online Food
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <a class="nav-item nav-link" href="#">Home</a>
              <a class="nav-item nav-link" href="notifiche_user.php">Notifiche</a>
              <a class="nav-item nav-link" href="ordini.php">Ordini</a>
              <a class="nav-item nav-link" href="carrello.php">Carrello</a>
              <a class="nav-item nav-link" id="logout" href="logout.php">Logout</a>
            </div>
          </nav>
        </div>
      </div>
      <div class="row main">
        <div class="col-sm-12">
          <main>
            <div class="row welcome">
              <div class="offset-sm-1 col-sm-10">
                  <section>
                    <h2 class="welcome">Benvenuto/a <?php echo $_SESSION['nome'] . " " . $_SESSION['cognome'] ?>!</h2>
                    <p class="welcome">In questa pagina puoi aggiungere al carrello tutti gli alimenti che più ti aggradano! Per vedere gli alimenti attualmente presenti nel carrello seleziona il link apposito.</p>
                    <p class="welcome">Seleziona il nome di un alimento per ottenere informazioni riguardanti il prezzo, gli ingredienti e le rimanenze disponibili.</p>
                    <p class="welcome">Premi il tasto 'Aggiungi' per aggiungere l'alimento al carrello. Se il tasto 'Aggiungi' di un certo alimento non è selezionabile significa che l'alimento è attualmente esaurito nei nostri magazzini.</p>
                  </section>
              </div>
            </div>
            <div class="row filter">
              <div class="offset-sm-3 col-sm-6">
                    <section>
                      <h3 class="food" >Filtra Cucina (non implementato)</h3>
                      <form id="filtri-alimenti" action="#" method="post">
                        <div id="containerfiltri">
                          <div class="divfiltri">
                            <input type="checkbox" id="tutte" checked>
                            <label for="tutte">Tutte</label>
                          </div>
                          <div class="divfiltri">
                            <input type="checkbox" id="italiana">
                            <label for="italiana">Italiana</label>
                          </div>
                          <div class="divfiltri">
                            <input type="checkbox" id="africana">
                            <label for="africana">Africana</label>
                          </div>
                          <div class="divfiltri">
                            <input type="checkbox" id="cinese">
                            <label for="cinese">Cinese</label>
                          </div>
                          <div class="divfiltri">
                            <input type="checkbox" id="giapponese">
                            <label for="giapponese">Giapponese</label>
                          </div>
                          <div class="divfiltri">
                            <input type="checkbox" id="indiana">
                            <label for="indiana">Indiana</label>
                          </div>
                          <div class="divfiltri">
                            <input type="checkbox" id="turca">
                            <label for="turca">Turca</label>
                          </div>
                        </div>
                        <button type="submit" class="btn btn-primary" name="filtra">Filtra</button>
                      </form>
                    </section>
              </div>
            </div>
            <div class="row food">
              <div class="offset-sm-1 col-sm-10">
                <section>
                  <h3 class="food">Alimenti</h3>
                    <ul class="nav" id="foodList"></ul>
                    <a class="btn btn-primary" id="vaialcarrello" href="carrello.php">Vai al carrello</a>
                </section>
              </div>
            </div>
          </main>
        </div>
      </div>
<div class="row footer">
  <div class="col-sm-12">
    <footer>
      Sito sviluppato da Pivato Francesco
    </footer>
  </div>
</div>
    </div>
  </body>
</html>
