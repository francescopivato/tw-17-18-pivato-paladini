        <?php

        if (session_status() == PHP_SESSION_NONE) { session_start(); }

        if (isset($_POST['nome']) &&
            isset($_POST['cognome']) &&
            isset($_POST['datanascita']) &&
            isset($_POST['email']) &&
            isset($_POST['password']))   {

            require("db_connection.php");
            require("use_db.php");

            $nome = $_POST['nome'];
            $cognome = $_POST['cognome'];
            $datanascita = $_POST['datanascita'];
            $email = $_POST['email'];
            $password = $_POST['password'];


            $sql_check_email = $conn->prepare("SELECT * FROM utenti WHERE(email=?)");
            $sql_check_email->bind_param("s", $email);
            $sql_check_email->execute();

            $result = $sql_check_email->get_result();

            if ($result->num_rows>0) {
              while ($row = $result->fetch_assoc()) {
                if ($email==$row['email']) {
                  header('Location: '. $_SERVER['HOST_NAME'] . '/tw-17-18-pivato-paladini/registrazione.php');
                  echo '<script type="text/javascript">',
                 'alert("L\'email inserita è già in uso.");',
                 '</script>';
                }
              }
            } else {
              //prepare statement
              $stmt = $conn->prepare("INSERT INTO utenti (nome, cognome, datanascita, email, password)
                                        VALUES (?, ?, ?, ?, ?)");

              //bind_param
              $stmt->bind_param("sssss", $nome, $cognome, $datanascita, $email, $password);

              if ($stmt->execute() == TRUE) {
                echo 'registration completed succesfully';
              } else {
                echo $stmt->error;
              }

              header('Location: '. $_SERVER['HOST_NAME'] . '/tw-17-18-pivato-paladini/login.php');

              $stmt->close();

            }

            $conn->close();

        }
        ?>
