<?php
  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  require('selezionaAlimenti.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Admin Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="controlla_notifiche.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="home-header">
            <figure>
              <img class="logo desktop" src="onlinefoodlogo.png" alt="Online Food Logo">
              <figcaption></figcaption>
            </figure>
            <figure>
              <img class="logo mobile" src="onlinefoodlogoMobile.png" alt="Online Food Logo">
              <figcaption></figcaption>
            </figure>
            <h1>Online Food</h1>
          </header>
        </div>
      </div>
      <div class="row nav">
        <div class="offset-sm-1 col-sm-10">
          <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
              <img src="onlinefoodlogoMobile.png" width="50" height="50" class="mx-auto" alt=""> Online Food
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <a class="nav-item nav-link" href="#">Home</a>
              <a class="nav-item nav-link" href="notifiche_admin.php">Notifiche</a>
              <a class="nav-item nav-link" href="listino.php">Listino</a>
              <a class="nav-item nav-link" href="logout.php">Logout</a>
            </div>
          </nav>
        </div>
      </div>
      <div class="row main">
        <div class="col-sm-12">
          <main>
            <div class="row welcome">
              <div class="offset-sm-1 col-sm-10">
                  <section>
                    <h2 class="welcome">Benvenuto/a Amministratore!</h2>
                    <p class="welcome">Ogni volta che aprirai questa pagina il sistema controllerà gli alimenti le cui porzioni sono uguali o inferiori a 10, inviandoti una notifica nel caso!</p>
                    <p class="welcome">Seleziona Listino dal menu per vedere tutti gli alimenti presenti in magazzino, per acquistarne ulteriori porzioni, per rimuoverli dal magazzino (in caso di problemi!) o per aggiungerne di nuovi!</p>
                  </section>
              </div>
            </div>
          </main>
        </div>
      </div>
<div class="row footer">
  <div class="col-sm-12">
    <footer>
      Sito sviluppato da Pivato Francesco
    </footer>
  </div>
</div>
    </div>
  </body>
</html>
