$(function(){

  if (localStorage.getItem("myCart") == null) {
    myCart = [];
  } else {
    myCart = JSON.parse(localStorage.getItem("myCart"));
  }

  $("#listaCarrello").empty();
  var sum = 0;
  console.log(myCart);

  for (var i = 0; i < myCart.length; i++) {

    var newcartitem = ("<li data value=\"" + myCart[i].id + "\" class=\"nav-cart-food\" id=\"cartItem" + myCart[i].id + "\">" + "</li>");

    $("#listaCarrello").append(newcartitem);

    var foodName = ("<h3 class=\"bg-dark text-light\">" + myCart[i].nome + "</h3>");
    var quantityLabel = ("<label for=\"quantita\"><strong>Quantità </strong></label>");
    var quantityController = ("<input type=\"number\" class=\"quantityCart\" id=\"quantita\" value=\"" + myCart[i].quantita + "\" min=\"1\" max=\"" + myCart[i].rimanenze + "\" >");
    var price = ("<p id=\"foodPrice" + myCart[i].id + "\" ><strong>" + "Prezzo: " + (myCart[i].prezzo * myCart[i].quantita).toFixed(2) + "€</strong></p>");
    var remove = ("<a class=\"removeFood\" href=\"#\">Rimuovi</a>");

    $("#cartItem" + myCart[i].id).append(foodName, quantityLabel, quantityController, price, remove);

    sum += (myCart[i].prezzo * myCart[i].quantita);
  } //close for

  $('#totalPrice').html("Prezzo totale: " + sum.toFixed(2) + "€");


  $(".quantityCart").change(function(){
    for (var i = 0; i < myCart.length; i++) {
      if (myCart[i].id == $(this).parent().attr("value")) {
        myCart[i].quantita = $(this).val();
        if (parseInt(myCart[i].quantita)>parseInt(myCart[i].rimanenze)) {
          myCart[i].quantita=myCart[i].rimanenze;
          $(this).val(myCart[i].quantita);
        } else {
          if (parseInt(myCart[i].quantita)<1) {
            myCart[i].quantita=1;
            $(this).val(myCart[i].quantita);
          }
        }
        localStorage.setItem("myCart", JSON.stringify(myCart));
        console.log(myCart);
        $("#foodPrice" + myCart[i].id).html("<strong>Prezzo: " + (myCart[i].prezzo * myCart[i].quantita).toFixed(2)  + "€</strong>");
        getTotalPrice();
        $('#totalPrice').html("Prezzo totale: " + sum.toFixed(2) + "€");
        return false;
      }
    }
  });

  $(".removeFood").click(function(){
    for (var i = 0; i < myCart.length; i++) {
      if (myCart[i].id == $(this).parent().attr("value")) {
        myCart.splice(i, 1);
        localStorage.setItem("myCart", JSON.stringify(myCart));
        $(this).parent().remove();
        console.log(myCart);
        getTotalPrice();
        $('#totalPrice').html("Prezzo totale: " + sum.toFixed(2) + "€");
        return false;
      }
    }
  });

  function getTotalPrice(){

    sum = 0;

    for (var i = 0; i < myCart.length; i++) {
      sum += (myCart[i].prezzo*myCart[i].quantita);
    }

  }

  $('#tasto_pagamento').click(function(){

    // e.preventDefault(); // button does not redirect to pagamento.php

    $.ajax({
        url: 'pagamento.php',
        type: 'POST',
        data: {'myCart' : localStorage.getItem("myCart")}, //
        success: function(response) {
            // console.log(response);
        }
    });
    localStorage.removeItem("myCart");
  })

  // $(".quantityCart").onchange

});
