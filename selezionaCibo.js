$(function(){

  $.getJSON("listaAlimenti.json", function(data, status){

    $('#foodList').empty();
    $('#containerfiltri').empty();

    var allFood = [];
    var myCart;
    var IDs;
    if (localStorage.getItem("myCart") == null) {
      myCart = [];
    } else {
      myCart = JSON.parse(localStorage.getItem("myCart"));
      // var tmpArray = []
      // for (var i = 0; i < myCart.length; i++) {
      //   tmpArray.push(myCart[i].id);
      // }
      // IDs = tmpArray;
      // console.log(IDs);
    }

    var found = false;

    for (var i = 0; i < data.length; i++) {

      found = false;

      allFood.push({
        "id" : parseInt(data[i].id), // id from sql is saved as string. parseInt converts it into a number
        "nome" : data[i].nome,
        "descrizione" : data[i].descrizione,
        "ingredienti" : data[i].ingredienti,
        "tipocucina" : data[i].tipocucina,
        "prezzo" : data[i].prezzo,
        "rimanenze" : data[i].rimanenze,
        "quantita" : 0
      });

      var id = parseInt(data[i].id); // id from sql is saved as string. parseInt converts it into a number
      var nome = data[i].nome;
      var descrizione = data[i].descrizione;
      var ingredienti = data[i].ingredienti;
      var tipocucina = data[i].tipocucina;
      var prezzo = data[i].prezzo;
      var rimanenze = parseInt(data[i].rimanenze);

      // var divfiltri = $("<div class=\"divfiltri\" id=\"divfiltri" + id + "\"></div>");

      console.log($("#filtri-alimenti"));

      // $("#filtri-alimenti").append("<label>Prova</label>"); //divfiltri

      // var checkboxfilter = "<input type=\"checkbox\" id=\"" + tipocucina + "\"><label for=\"" + tipocucina + "\">" + tipocucina + "</label>";
      //
      // $("#divfiltri" + id).append(checkboxfilter);


      // <div class="divfiltri">
      //   <input type="checkbox" id="tutte" checked>
      //   <label for="tutte">Tutte</label>
      // </div>

      // $('#filtri-alimenti').append();

      var newli = $( "<li class=\"nav-item-food\" id=\"listItem" + id + "\"></li>" );

      $( "#foodList" ).append( newli );

      var buttonName = $("<button type=\"button\" class=\"btn btn-dark text-white btn-lg button-name\" data-html=\"true\" data-toggle=\"popover\" title=\"Prezzo: " + prezzo + "€" + "\" data-placement=\"top\" data-content=\"Ingredienti: " + ingredienti + "<br/>" + "Descrizione: " + descrizione + "<br/>" + "Cucina: " + tipocucina + "<br/>" + "Rimanenze: " + rimanenze + "\">" + nome + "</button>");

      var buttonAdd = $("<button value = \"" + id + "\" id=\"buttonAdd" + id + "\" type=\"button\" class=\"btn btn-light btn-lg buttonAggiungi\" name=\"button\">Aggiungi</button>");

      if (rimanenze <= 0) {
        $(buttonAdd).prop("disabled",true); //buttonAdd is disabled
      } else {
        $(buttonAdd).prop("disabled",false); //buttonAdd is enabled
      }

      $(buttonAdd).click(function(){
            //salvo in una variabile tmp il cibo di cui ho appena cliccato il tasto 'Aggiungi'
            var tmp = allFood[$(this).attr("value") - 1]; //SQL IDs start from 1 while array in jquery starts from 0 -> that's why I put -1

              found = false;
              var j = 0;

              //devo aggiungere il cibo selezionato all'interno del mio carrello ma prima controllo se il carrello lo contiene già

              while (j < myCart.length) { //ciclo tutto l'array di oggetti myCart

                if (found == false) { //se l'alimento non è ancora stato trovato
                  if (myCart[j].id == tmp.id) { //se l'alimento corrente è giá presente in myCart
                    found = true; //dico che ho trovato l'alimento
                    myCart[j].quantita++; // ne aumento semplicemente la quantità
                    console.log(myCart);
                    localStorage.setItem("myCart", JSON.stringify(myCart));
                    if ((myCart[j].rimanenze - myCart[j].quantita) <= 0) { // if rimanenze - quantita <= 0
                      // console.log("sono furbo e disabilito il bottone dato che rimanenze - quantita = " + myCart[j].rimanenze + " - " + myCart[j].quantita);
                      $(this).prop("disabled",true); //buttonAdd is disabled
                    } else {
                      // console.log("continuo per la mia strada tanto rimanenze - quantita = " + myCart[j].rimanenze + " - " + myCart[j].quantita);
                      $(this).prop("disabled",false); // else is enabled
                    }
                    return false; //esco dal while
                  }
                }
                j++;
              }

              if (!found) { //se l'alimento non è stato trovato in myCart
                myCart.push(tmp); //lo aggiungo in questo momento
                myCart[myCart.length-1].quantita = 1; // setto la sua quantita = 1 (di base è = 0)
                console.log(myCart);
                localStorage.setItem("myCart", JSON.stringify(myCart));
                if ((myCart[myCart.length-1].rimanenze - myCart[myCart.length-1].quantita) <= 0) { // if rimanenze <= 0
                  // console.log("sono furbo e disabilito il bottone dato che rimanenze - myCart quantita = " + myCart[myCart.length-1].rimanenze + " - " + myCart[myCart.length-1].quantita);
                  $(this).prop("disabled",true); //buttonAdd is disabled
                } else {
                  // console.log("continuo per la mia strada tanto rimanenze - my Cart quantita = " + myCart[myCart.length-1].rimanenze + " - " + myCart[myCart.length-1].quantita);
                  $(this).prop("disabled",false); // else is enabled
                }
              }

              // j = 0;
              // found = false;

      }); // close button function

      $( "#listItem" + id ).append(buttonName, buttonAdd);

    } // close for

    if (myCart.length != 0) {
      for (var i = 0; i < myCart.length; i++) {
        // console.log("le rimanenze e le quantita di " + myCart[i].nome + " sono rispettivamente " + myCart[i].rimanenze + " e " + myCart[i].quantita);
        if ((parseInt(myCart[i].rimanenze) - myCart[i].quantita) <= 0) {
          // console.log("sono bravo e disattivo il bottone per " + myCart[i].nome);
          $('#buttonAdd' + myCart[i].id).prop("disabled",true);
        } else {
          // console.log("qui invece abbiamo rimanenze a sufficienza quindi rimane attivo il bottone di " + myCart[i].nome);
          $('#buttonAdd' + myCart[i].id).prop("disabled",false);
        }
      }

    }

  }); // close getJSON
}); // close function
