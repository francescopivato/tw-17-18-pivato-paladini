# README #

Progetto di Tecnologie Web 2017-18.

# IMPORT DATABASE #

* Instructions for WAMP
* Open PHPMyAdmin
* On the left side click "New" to create a new database
* Call the database 'onlinefood' and choose as Character Encoding 'UTF8_general_ci'
* Click the tab 'Import' at the top of the page
* Click 'Choose file' in 'File to Import' section
* Select onlinefood.sql
* Click 'Execute' at the bottom of the page
* The whole database is now ready

# INSTRUCTIONS #

* Website starts from login.php
* If you want to login as admin, use: email="admin@admin.com" ; password="admin"
* If you want to login as user, you need to register first

# DATABASE AND TABLES #

Database Name : onlinefood

Table Name : utenti
Field Names : id, nome, cognome, datanascita, email, password

Table Name : alimenti
Field Names : id, nome, descrizione, ingredienti, tipocucina, prezzo

Table Name : notifiche
Field Names : id, utente.id, messaggio, letto, data

Table Name : ordini (not implemented yet)
Field Names : id, utenti.id, alimentiordinati[alimenti.id, alimenti.nome, alimenti.prezzo], prezzototale, dataora

### Author ###

* Pivato Francesco
