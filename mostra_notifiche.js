$(function(){

  $.ajax({
    url: 'mostra_notifiche.php',
    type: 'POST',
    // data: '',
    success: function(response){
      // console.log(response);
      if (response!="false") {
        $("#elencoNotifiche").prepend(response);
      } else {
        $("#elencoNotifiche").prepend("<p class=\"no-notification\">Nessuna notifica.</p>");
      }
    }
  });

});
