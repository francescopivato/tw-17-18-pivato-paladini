<?php if (session_status() == PHP_SESSION_NONE) { session_start(); } ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrazione</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="login-header">
            <figure>
              <img class="logo desktop" src="onlinefoodlogo.png" alt="Online Food Logo">
              <figcaption></figcaption>
            </figure>
            <figure>
              <img class="logo mobile" src="onlinefoodlogoMobile.png" alt="Online Food Logo">
              <figcaption></figcaption>
            </figure>
            <h1 class="login-h1">Online Food</h1>
          </header>
        </div>
      </div>
      <div class="row main">
        <div class="col-sm-12">
          <main>
              <div class="row introduction">
                <div class="offset-sm-2 col-sm-8">
                  <p>Benvenuto nel processo di registrazione.</p>
                  <p> Per favore inserisci i seguenti dati:</p>
                </div>
              </div>
              <form action="registrazione_script.php" method="post">
                <fieldset>
                  <div class="row legend">
                    <div class="offset-sm-2 col-sm-8">
                      <legend>Dati Anagrafici</legend>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="nome" class="col-sm-2 col-form-label sr-only">Nome: </label>
                    <div class="offset-sm-2 col-sm-8">
                      <input type="text" id="nome" name="nome" class="form-control" placeholder="Nome" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="cognome" class="col-sm-2 col-form-label sr-only">Cognome: </label>
                    <div class="offset-sm-2 col-sm-8">
                      <input type="text" id="cognome" name="cognome" class="form-control" placeholder="Cognome" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="datanascita" class="col-sm-2 col-form-label sr-only">Data di Nascita: </label>
                    <div class="offset-sm-2 col-sm-8">
                      <input type="date" id="datanascita" name="datanascita" min="1918-01-01" max="2000-12-31"
                      class="form-control" placeholder="Data Nascita" required>
                    </div>
                  </div>
                </fieldset>
                <fieldset>
                  <div class="row legend">
                    <div class="offset-sm-2 col-sm-8">
                      <legend>Dati Utente</legend>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label sr-only">Email: </label>
                    <div class="offset-sm-2 col-sm-8">
                      <input type="email" id="email" name="email" class="form-control" placeholder="Email" required>
                      <div class="invalid-tooltip">
                        Email already in use.
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label sr-only">Password: </label>
                    <div class="offset-sm-2 col-sm-8">
                      <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
                    </div>
                  </div>
                </fieldset>
                <div class="form-group row">
                  <div class="offset-sm-2 col-sm-8">
                    <input class="btn btn-lg btn-secondary" type="submit" id="registrati" value="Registrati">
                    <input class="btn btn-lg btn-secondary" type="reset" id="reset" value="Reset">
                  </div>
                </div>
              </form>
          </main>
        </div>
      </div>
<div class="row footer">
  <div class="col-sm-12">
    <footer>
      Sito sviluppato da Pivato Francesco
    </footer>
  </div>
</div>
    </div>
  </body>
</html>
