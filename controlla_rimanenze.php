<?php

  if (session_status() == PHP_SESSION_NONE) { session_start(); }


  if ($_SESSION['id']==1) { // if admin is logged

    echo "true";

    require("db_connection.php");
    require("use_db.php");

    $limite_rimanenze = 10;

    $stmt_select = $conn->prepare("SELECT id, nome, rimanenze FROM alimenti WHERE (rimanenze<=?)");

    $stmt_select->bind_param("i", $limite_rimanenze);
    $stmt_select->execute();
    $stmt_select->store_result();
    $stmt_select->bind_result($id, $nome, $rimanenze);

    if ($stmt_select->num_rows>0) {
      while ($stmt_select->fetch()) {

        date_default_timezone_set("Europe/Rome");

        $id_user = $_SESSION['id'];
        $messaggio = "Attenzione! Rimangono " . $rimanenze . " porzioni di " . $nome . " (ID: " . $id . "). Controlla il listino per maggiori informazioni.";
        $letto = 0;
        $data = date("Y-m-d") . " " . date("G:i:s");

        $stmt_check_message = $conn->prepare("SELECT messaggio FROM notifiche WHERE utente_id=?");
        $stmt_check_message->bind_param("i", $id_user);
        $stmt_check_message->execute();
        $stmt_check_message->store_result();
        $stmt_check_message->bind_result($m);

        $condition = false;

        if ($stmt_check_message->num_rows > 0) {
          while ($stmt_check_message->fetch()) {
            if ($messaggio == $m) {
              $condition = true;
              echo "false";
              break;
            }
          }
        }

        $stmt_check_message->free_result();

        if (!$condition) {
          $stmt_insert_notification = $conn->prepare("INSERT INTO notifiche (utente_id, messaggio, letto, data) VALUES (?, ?, ?, ?)");
          $stmt_insert_notification->bind_param("isis", $id_user, $messaggio, $letto, $data);
          $stmt_insert_notification->execute();
          $stmt_insert_notification->close();
        } else {
          echo "false";
        }

        //non funziona! le notifiche vengono ripetute ogni volta che sono su home_admin
        // if ($messaggio != $m) { // se non esiste già una notifica con lo stesso messaggio
        //
        //   $stmt_insert_notification = $conn->prepare("INSERT INTO notifiche (utente_id, messaggio, letto, data) VALUES (?, ?, ?, ?)");
        //   $stmt_insert_notification->bind_param("isis", $id_user, $messaggio, $letto, $data);
        //   $stmt_insert_notification->execute();
        //   $stmt_insert_notification->close();
        //
        // } else {
        //   echo "false";
        // }
      }
    } else {
      echo "false";
    }

    $stmt_select->close();

    $conn->close();

  } else {
    echo "false";
  }

?>
