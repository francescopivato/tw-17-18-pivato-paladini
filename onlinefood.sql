-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Creato il: Feb 07, 2018 alle 09:13
-- Versione del server: 5.7.19
-- Versione PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onlinefood`
--

-- -- --------------------------------------------------------

--
-- Struttura della tabella `notifiche`
--

DROP TABLE IF EXISTS `notifiche`;
CREATE TABLE IF NOT EXISTS `notifiche` (
  `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `utente_id` INT(6) NOT NULL,
  `messaggio` VARCHAR(300) NOT NULL,
  `letto` TINYINT(1) NOT NULL,
  `data` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `notifiche`
--


-- --------------------------------------------------------

--
-- Struttura della tabella `alimenti`
--

DROP TABLE IF EXISTS `alimenti`;
CREATE TABLE IF NOT EXISTS `alimenti` (
  `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `descrizione` varchar(300) NOT NULL,
  `ingredienti` varchar(300) NOT NULL,
  `tipocucina` varchar(30) NOT NULL,
  `prezzo` float(4,2) DEFAULT NULL,
  `rimanenze`int(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `alimenti`
--

INSERT INTO `alimenti` (`id`, `nome`, `descrizione`, `ingredienti`, `tipocucina`, `prezzo`, `rimanenze`) VALUES
(1, 'Pizza Margherita', 'Pizza margherita cucinata secondo la ricetta napoletana', 'Farina, lievito di birra, sale, acqua, olio extravergine d\'oliva, passata di pomodoro, mozzarella, basilico', 'Italiana', 5.00, 100),
(2, 'Pizza Boscaiola', 'Pizza ai funghi porcini cucinata secondo la ricetta napoletana', 'Farina, lievito di birra, sale, acqua, olio extravergine d\'oliva, passata di pomodoro, mozzarella, funghi porcini', 'Italiana', 6.50, 100),
(3, 'Pizza Marinara', 'Pizza con pomoro e origano cucinata secondo la ricetta napoletana', 'Farina, lievito di birra, sale, acqua, olio extravergine d\'oliva, passata di pomodoro, origano,', 'Italiana', 4.50, 100),
(4, 'Pizza Napoli', 'Pizza con acciughe cucinata secondo la ricetta napoletana', 'Farina, lievito di birra, sale, acqua, olio extravergine d\'oliva, passata di pomodoro, mozzarella,acciughe', 'Italiana', 6.50, 100),
(5, 'Pizza Prosciutto', 'Pizza al prosciutto cotto cucinata secondo la ricetta napoletana', 'Farina, lievito di birra, sale, acqua, olio extravergine d\'oliva, passata di pomodoro, mozzarella, prosciutto cotto', 'Italiana', 6.50, 100),
(6, 'Panino Kebab', 'Kebab cucinato secondo la ricetta turca', 'Farina 00, olio, sale, carne, insalata, salsa yogurt, salsa piccante, patatine fritte, cipolla, pomodoro', 'Turca', 4.00, 100),
(7, 'Cuscus', 'Cuscus cucinato secondo la ricetta africana', 'Couscous, Melanzane, Peperoni, Zucchine, Cipolla Bianca, Pomodori, Prezzemolo, Basilico, Olio extravergine d\'oliva', 'Africana', 6.00, 100),
(8, 'Riso alla Cantonese', 'Riso alla Cantonese cucinato secondo la ricetta cinese', 'Riso, Acqua, Sale, Pisellini, Prosciutto Cotto, Cipollotto Fresco, Uova, Olio di semi, Salsa di soia, Sale', 'Cinese', 6.50, 100),
(9, 'Sushi', 'Sushi cucinato secondo la ricetta giapponese', 'Filetti di Pesce, Riso sushi, Alghe Nori, Aceto di Riso, Salsa di Soia, Wasabi, Zenzero sott\'Aceto, Stuoia di Bambu\', Frutta e Verdura', 'Giapponese', 5.50, 100),
(10, 'Pollo al Curry', 'Pollo al curry cucinato secondo la ricetta indiana', 'Sopracosce di pollo, Radice di zenzero, Mela golden, Latte di cocco o panna, Curry, Cipolla, Porri, Olio di semi di arachide, Sale', 'Indiana', 8.00, 100),
(11, 'Piadina Vuota', 'Piadina romagnola cucinata secondo la ricetta riminese', 'Farina 00, olio, sale, strutto, acqua', 'Italiana', 3.00, 100),
(12, 'Piadina con Salame', 'Piadina romagnola cucinata secondo la ricetta riminese', 'Farina 00, olio, sale, strutto, acqua, salame', 'Italiana', 3.50, 100),
(13, 'Piadina con Prosciutto', 'Piadina romagnola cucinata secondo la ricetta riminese', 'Farina 00, olio, sale, strutto, acqua, prosciutto', 'Italiana', 3.50, 100),
(14, 'Piadina con Salsiccia', 'Piadina romagnola cucinata secondo la ricetta riminese', 'Farina 00, olio, sale, strutto, acqua, salsiccia', 'Italiana', 3.50, 100),
(15, 'Spianata Squacquerone', 'Spianata con squacquerone e rucola fresca', 'Farina, lievito di birra, sale, acqua, squacquerone', 'Italiana', 3.50, 100);

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

DROP TABLE IF EXISTS `utenti`;
CREATE TABLE IF NOT EXISTS `utenti` (
  `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  `cognome` varchar(30) NOT NULL,
  `datanascita` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`id`, `nome`, `cognome`, `datanascita`, `email`, `password`) VALUES
(1, 'admin', 'admin', '1980-01-01', 'admin@admin.com', 'admin'),
(2, 'Francesco', 'Pivato', '1995-10-14', 'francesco.pivato@studio.unibo.it', 'francesco'),
(3, 'Mario', 'Rossi', '1996-08-11', 'mario.rossi@studio.unibo.it', 'mario'),
(4, 'Carlo', 'Verdi', '1994-01-22', 'carlo.verdi@studio.unibo.it', 'carlo'),
(5, 'Gino', 'Bianchi', '1997-06-10', 'gino.bianchi@studio.unibo.it', 'gino');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
