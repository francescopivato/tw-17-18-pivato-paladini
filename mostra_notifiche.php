<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

require("db_connection.php");
require("use_db.php");

$result;

$id_current_user = $_SESSION['id'];

$stmt_select = $conn->prepare("SELECT messaggio, letto, data FROM notifiche WHERE utente_id=? ORDER BY data DESC");

$stmt_select->bind_param("i", $id_current_user);
$stmt_select->execute();
$stmt_select->store_result();
$stmt_select->bind_result($messaggio, $letto, $data);


if ($stmt_select->num_rows>0) {

    while ($stmt_select->fetch()) {

      if ($letto == 1) {
        echo "<li class=\"notification-item\"><h4 class=\"bg-dark text-light\">Messaggio</h4><p class=\"messaggio-notifica\">" . $data . " : " . $messaggio . "</p></li>";
      } else {
        echo "<li class=\"notification-item\"><h4 class=\"bg-dark text-light\">Messaggio (da leggere!)</h4><p class=\"messaggio-notifica\">" . $data . " : " . $messaggio . "</p></li>";
      }

      // <h4 class=\"bg-dark text-light\">Messaggio Letto:</h4><p class=\"lettura-notifica\">" . $letto . "</p>

    }
    $stmt_select->free_result();

    $stmt_update = $conn->prepare("UPDATE notifiche SET letto='1' WHERE utente_id=?");
    $stmt_update->bind_param("i", $id_current_user);
    $stmt_update->execute();


    $stmt_update->close();
} else {
  echo "false"; // no notification at all
}

$stmt_select->close();
$conn->close();

?>
