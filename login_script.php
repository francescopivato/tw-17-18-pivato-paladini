<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

if (isset($_POST['email']) && isset($_POST['password']))   {

    require("db_connection.php");

    $email = $_POST['email'];
    $password = $_POST['password'];

    require("use_db.php");

    $stmt = $conn->prepare("SELECT * FROM utenti WHERE (email=?) AND (password=?)");

    $stmt->bind_param("ss", $email, $password);

    $stmt->execute();

    $result = $stmt->get_result();

    if ($result->num_rows == 1) {

        $row = $result->fetch_assoc();
        $_SESSION['id'] = $row["id"];
        $_SESSION['nome'] = $row["nome"];
        $_SESSION['cognome'] = $row["cognome"];
        $_SESSION['datanascita'] = $row["datanascita"];
        $_SESSION['email'] = $row["email"];

        $_SESSION['account'] = $_POST['email'];

        //login riuscito
        $_SESSION['result'] = true;



        if ($_SESSION['email'] == 'admin@admin.com') {
          header('Location: '. $_SERVER['HOST_NAME'] . '/tw-17-18-pivato-paladini/home_admin.php');
        } else {
          header('Location: '. $_SERVER['HOST_NAME'] . '/tw-17-18-pivato-paladini/home_user.php');
        }

    } else {
        // credenziali errate
        $_SESSION['result'] = false;
        header('Location: '. $_SERVER['HOST_NAME'] . '/tw-17-18-pivato-paladini/login.php');
    }

    $result->close();
    $stmt->close();
    $conn->close();

}

?>
