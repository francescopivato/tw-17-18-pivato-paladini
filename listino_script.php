<?php

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  require('db_connection.php');
  require('use_db.php');

  if (isset($_POST['ordinaAlimento'])) {

    if ($_POST['quantitadaordinare'] > 0) { // maybe ordina was clicked when quantity was set to zero
      $stmt_update = $conn->prepare("UPDATE alimenti SET rimanenze = rimanenze + ? WHERE id=?");
      $stmt_update->bind_param("ii", $_POST['quantitadaordinare'], $_POST['id']);
      if ($stmt_update->execute()) {
        $stmt_insert_notification = $conn->prepare("INSERT INTO notifiche (utente_id, messaggio, letto, data) VALUES (?, ?, ?, ?)");

        date_default_timezone_set("Europe/Rome");

        $id_user = $_SESSION['id'];
        $message = "Ordine di " . $_POST['nome'] . " (" . $_POST['quantitadaordinare'] . "pz) arrivato al magazzino.";
        $letto = 0;
        $data = date("Y-m-d") . " " . date("G:i:s");

        $stmt_insert_notification->bind_param("isis", $id_user, $message, $letto, $data);
        $stmt_insert_notification->execute();
        $stmt_insert_notification->close();

      }
      $stmt_update->close();
    }

  } else {
    if (isset($_POST['rimuoviAlimento'])) {

      $stmt_remove = $conn->prepare("DELETE FROM alimenti WHERE id=?");
      $stmt_remove->bind_param("i", $_POST['id']);
      if ($stmt_remove->execute()) {
        $stmt_insert_notification = $conn->prepare("INSERT INTO notifiche (utente_id, messaggio, letto, data) VALUES (?, ?, ?, ?)");

        date_default_timezone_set("Europe/Rome");

        $id_user = $_SESSION['id'];
        $message = "Alimento " . $_POST['nome'] . " rimosso dal magazzino.";
        $letto = 0;
        $data = date("Y-m-d") . " " . date("G:i:s");

        $stmt_insert_notification->bind_param("isis", $id_user, $message, $letto, $data);
        $stmt_insert_notification->execute();
        $stmt_insert_notification->close();
      }
      $stmt_remove->close();
    } else {
      echo "nessun botton premuto";
    }
  }

  $conn->close();

  header('Location: '. $_SERVER['HOST_NAME'] . '/tw-17-18-pivato-paladini/listino.php');

?>
