<?php

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  require('selezionaAlimenti.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Listino</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="listino.js"></script>
    <script type="text/javascript" src="controlla_notifiche.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="home-header">
            <figure>
              <img class="logo desktop" src="onlinefoodlogo.png" alt="Online Food Logo">
              <figcaption></figcaption>
            </figure>
            <figure>
              <img class="logo mobile" src="onlinefoodlogoMobile.png" alt="Online Food Logo">
              <figcaption></figcaption>
            </figure>
            <h1>Online Food</h1>
          </header>
        </div>
      </div>
      <div class="row nav">
        <div class="offset-sm-1 col-sm-10">
          <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
              <img src="onlinefoodlogoMobile.png" width="50" height="50" class="mx-auto" alt=""> Online Food
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <a class="nav-item nav-link" href="home_admin.php">Home</a>
              <a class="nav-item nav-link" href="notifiche_admin.php">Notifiche</a>
              <a class="nav-item nav-link" href="#">Listino</a>
              <a class="nav-item nav-link" href="logout.php">Logout</a>
            </div>
          </nav>
        </div>
      </div>
      <div class="row main">
        <div class="col-sm-12">
          <main>
            <div class="row food">
              <div class="offset-sm-1 col-sm-10">
                <section>
                  <h2 class="titoloPagina">Listino</h2>
                  <p>Premi Aggiungi Alimento per aggiungere un alimento al database.</p>
                    <p>Più in basso troverai informazioni legate agli alimenti già presenti nel database,
                    in particolare potrai controllare le rimanenze di ciascuno di essi, e nel caso, potrai decidere
                    di ordinarne ancora selezionando prima la quantità (massimo 100 per ordine) e poi premendo
                    Ordina Alimento.</p>
                    <p>Oppure puoi premere Rimuovi Alimento se vuoi rimuovere un alimento dal database.</p>
                  <a href="aggiungiAlimento.php" class="btn btn-primary buttonAggiungiAlimento">Aggiungi Alimento</a>
                  <h3><strong>Lista Alimenti: </strong></h3>
                  <ul id="listino">
                    <!-- <li class="listino-item">
                      <form class="listino-item-form" action="listino_script.php" method="post">
                        <div class="div-listino-item">
                          <label for="id">ID: </label>
                          <input type="text" id="id" value="999" class="listino-item-id" disabled>
                        </div>
                        <div class="div-listino-item">
                          <label for="nome">Nome: </label>
                          <input type="text" id="nome" value="Spianata Squacquerone" class="listino-item-nome" disabled>
                        </div>
                        <div class="div-listino-item">
                          <label for="rimanenze">Rimanenze: </label>
                          <input type="text" id="rimanenze" value="999" class="listino-item-rimanenze" disabled>
                        </div>
                        <div class="div-listino-item">
                          <label for="quantitadaordinare">Quantità da ordinare: </label>
                          <input type="number" min="0" max="100" id="quantitadaordinare" class="listino-item-quantitadaordinare" value="100">
                        </div>
                        <div class="div-listino-item">
                          <input class="btn btn-dark listino-item-ordina" type="submit" name="ordinaAlimento" value="Ordina">
                          <input class="btn btn-dark listino-item-rimuovi" type="submit" name="rimuoviAlimento" value="Rimuovi">
                        </div>
                      </form>
                    </li> -->
                  </ul>
                </section>
              </div>
            </div>
          </main>
        </div>
      </div>
<div class="row footer">
  <div class="col-sm-12">
    <footer>
      Sito sviluppato da Pivato Francesco
    </footer>
  </div>
</div>
    </div>
  </body>
</html>
